# bella_models_nuxt

> Criada em 2006 e com duas sedes no Rio de Janeiro, a Bella Models é uma das mais fortes agências do Rio de Janeiro, prestando um serviço diferenciado no mercado fashion, comercial e plus size.

## Build Setup

``` bash
# install dependencies
$ yarn

# serve with hot reload at localhost:7000
$ yarn dev

# build for production and launch server
# don't use Vercel
$ yarn build && yarn generate
$ yarn start
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
